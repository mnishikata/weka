//
//  Array.swift
//  Tango
//
//  Created by Masatoshi Nishikata on 8/03/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation

extension Array {
	mutating func shuffle() {
		for i in 0 ..< count {
			// Select a random element between i and end of array to swap with.
			let nElements = count - i
			let n = Int(arc4random_uniform( UInt32(nElements))) + i
			(self[i], self[n]) = (self[n], self[i])
		}
	}
}

extension NSMutableArray {
	func shuffle() {
		let count: Int = self.count
		for i in 0 ..< count {
			// Select a random element between i and end of array to swap with.
			let nElements = count - i
			let n = Int(arc4random_uniform( UInt32(nElements))) + i
			(self[i], self[n]) = (self[n], self[i])
		}
	}
}