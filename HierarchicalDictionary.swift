//
//  Created by Masatoshi Nishikata on 2/03/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//
import Foundation

// Organize NSMutableDictionary subitems in 'items'
typealias HierarchicalDictionary = NSMutableDictionary
extension NSMutableDictionary  {
	
	var items: NSMutableArray? {
		get {
			return object(forKey: "items") as? NSMutableArray
		}
		set {
			if newValue != nil { setObject( newValue!, forKey: "items" as NSString) }
			else { removeObject(forKey: "items") }
		}
	}
	
	func addItem(_ item: NSMutableDictionary) {
		assert( Thread.isMainThread == true, "** CALL ME ON MAIN THREAD! ** ")

		if items == nil {
			let marray = NSMutableArray()
			setObject(marray, forKey: "items" as NSString)
		}
		items!.add(item)
	}
	
	func insertItem(_ item: NSMutableDictionary, atIndex index: Int) {
		assert( Thread.isMainThread == true, "** CALL ME ON MAIN THREAD! ** ")

		if items == nil {
			let marray = NSMutableArray()
			setObject(marray, forKey: "items" as NSString)
		}
		items!.insert( item, at: index)
	}
	
	func removeItemAtIndex( _ index: Int ) {
		assert( Thread.isMainThread == true, "** CALL ME ON MAIN THREAD! ** ")

    if items != nil && items!.count > index {
      items!.removeObject(at: index)
    }
  }
  
	
	// Find item recursively and remove it
	func removeAndDeleteForItem( _ item: NSMutableDictionary ) -> Bool {
		assert( Thread.isMainThread == true, "** CALL ME ON MAIN THREAD! ** ")

		if items == nil { return false }
		
		let index = items!.index(of: item)
		
		if index != NSNotFound  {
			items!.removeObject(at: index);
			return true
		}
		
		if let itemsCopy = items!.copy() as? NSArray { // COPY FIRST TO BE THREAD SAFE
			for subitem in itemsCopy {
				let flag = (subitem as? NSMutableDictionary)?.removeAndDeleteForItem(item)
				if flag == true { return true }
			}
		}
		
		return false
	}
	
	
	// Find a direct parent of 'self' in item
	func findParentIn(_ item: NSMutableDictionary) ->  NSMutableDictionary? {
		
		if item == self { return nil }
		if item.items == nil { return nil }
		
		if (item.items!).index( of: self ) != NSNotFound { return item }
		
		if let itemsCopy = item.items!.copy() as? NSArray { // COPY FIRST TO BE THREAD SAFE
			for subitem in itemsCopy {
				let result = findParentIn(subitem as! NSMutableDictionary)
				if result != nil { return result }
			}
		}
		return nil
	}
	
	// 'condition' should return a object that was obtained from 'item'
	func collectWithCondition( _ condition: (_ item: NSMutableDictionary)-> Any? ) -> [Any] {
		
		var toArray: [Any] = []
		
		// Collect items according to 'condition'
		
		if let object: Any = condition(self) {
			toArray.append( object )
		}
		
		if let itemsCopy = items?.copy() as? NSArray { // COPY FIRST TO BE THREAD SAFE
			for item in itemsCopy {
				if let array = (item as? NSMutableDictionary)?.collectWithCondition(condition) {
					toArray += array
				}
			}
		}
		return toArray
	}
	
	func makeObjectsPerformBlock( _ handler: (_ item: NSMutableDictionary)->Void  ) {
		
		handler(self)
		
		if let itemsCopy = items?.copy() as? NSArray { // COPY FIRST TO BE THREAD SAFE
			for item in itemsCopy {
				(item as! NSMutableDictionary).makeObjectsPerformBlock( handler )
			}
		}
	}
	
	// Look for a single item that matches with 'condition'
	func lookForWithCondition( _ condition: (_ item: NSMutableDictionary)->Bool ) -> NSMutableDictionary? {
		
		if condition(self) == true { return self }
		
		if let itemsCopy = items?.copy() as? NSArray { // COPY FIRST TO BE THREAD SAFE
			for item in itemsCopy {
				if let object = (item as! NSMutableDictionary).lookForWithCondition(condition) {
					return object
				}
			}
		}
		
		return nil
	}
	
	func indexOfItem(_ item: NSMutableDictionary) ->  Int {
		if items == nil { return NSNotFound }
		let index = items!.index( of: item )
		
		return index
	}
}

