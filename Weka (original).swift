//
//  Created by Masatoshi Nishikata on 2/03/16.
//  Copyright © 2016 Catalystwo. All rights reserved.
//

import Foundation
import Darwin
import UIKit

func WLoc( title:String ) -> String {
	return NSLocalizedString(title, comment: "" )
}

func WLoc( title:String, withArguments args:CVarArgType) -> String {
	return NSString(format: NSLocalizedString(title, comment: "" ), args) as String
}

#if os(iOS) || os(watchOS)
	
	func RGBA(r:CGFloat,_ g:CGFloat,_ b:CGFloat,_ a:CGFloat) -> UIColor  {
		return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
	}
#else
	func RGBA(r:CGFloat,_ g:CGFloat,_ b:CGFloat,_ a:CGFloat) -> UIColor  {
		return NSColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
	}
#endif


func showError( error:NSError! ) {
	print("error \(error.localizedDescription)")
}

//func displayErrorAlertIfNecessary( error:NSError? ) {
//	if error == nil { return }
//	
//	if NSThread.isMainThread() == false {
//		print(" (*_*) Please call me on main thread" )
//		return
//	}
//	
//	#if os(iOS)
//		
//		let alert = UIAlertView(title: error!.localizedDescription, message: "", delegate: nil, cancelButtonTitle: "OK")
//		alert.show()
//		
//	#elseif os(OSX)
//		let message = error!.localizedDescription
//		
//		let alert = NSAlert()
//		alert.messageText = "Oops"
//		alert.informativeText = message
//		alert.alertStyle = NSAlertStyle.CriticalAlertStyle
//		alert.addButtonWithTitle(WLoc("OK"))
//		alert.runModal()
//		
//	#endif
//
//}
#if os(iOS)
	
	func displayErrorAlertIfNecessary( error:NSError?, inViewController viewController:UIViewController ) {
		if error == nil { return }
		
		if NSThread.isMainThread() == false {
			print(" (*_*) Please call me on main thread" )
			return
		}
		
		ShowAlertMessage( error!.localizedDescription, "", viewController )
		
	}
#elseif os(OSX)
	func displayErrorAlertIfNecessary( error:NSError? ) {
	if error == nil { return }

	if NSThread.isMainThread() == false {
		print(" (*_*) Please call me on main thread" )
		return
	}
	let message = error!.localizedDescription

	let alert = NSAlert()
	alert.messageText = "Oops"
	alert.informativeText = message
	alert.alertStyle = NSAlertStyle.CriticalAlertStyle
	alert.addButtonWithTitle(WLoc("OK"))
	alert.runModal()
}
#endif

func shortUUIDString() -> String {
	
	let base64TailBuffer = "="
	let tempUuid = NSUUID()
	var tempUuidBytes = [UInt8](count: 16, repeatedValue: 0)
	tempUuid.getUUIDBytes(&tempUuidBytes)
	let data = NSData(bytes: &tempUuidBytes, length: 16)
	let base64 = data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
	
	let string = base64.stringByReplacingOccurrencesOfString("/", withString: "_", options: [], range: nil)
	
	return string.stringByReplacingOccurrencesOfString(base64TailBuffer, withString: "", options: [], range: nil)
}


//func displayMessage( title:String ) {
//	
//	#if os(iOS)
//		
//		let alert = UIAlertView(title: title, message: "", delegate: nil, cancelButtonTitle: "OK")
//		alert.show()
//		
//	#elseif os(OSX)
//		
//		let alert = NSAlert()
//		alert.messageText = title
//		alert.informativeText = ""
//		alert.alertStyle = NSAlertStyle.CriticalAlertStyle
//		alert.addButtonWithTitle(WLoc("OK"))
//		alert.runModal()
//		
//	#endif
//	
//}

#if os(OSX)
	
	func loadingImages(tintColor:NSColor) -> [NSImage] {
		
		var images = [NSImage]()
		let size = NSMakeSize(30.0, 30.0);
		let fraction:Float =  0 - 1.0/36.0 * 360.0
		
		for i in 0...36 {
			
			let image = NSImage(size:size)
			image.lockFocus()
			
			let ctx = NSGraphicsContext.currentContext()!.CGContext
			
			let point = CGPointMake(15,15)
			let radius = CGFloat(12.0)
			let startAngle = CGFloat(Float(i) * fraction)
			let endAngle = CGFloat(fraction * 33) + startAngle
			
			let bezier = NSBezierPath()
			bezier.appendBezierPathWithArcWithCenter( point, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
			
			bezier.lineWidth = 2.0
			CGContextSetStrokeColorWithColor(ctx, tintColor.CGColor)
			CGContextSetLineCap( ctx,  CGLineCap.Butt)
			bezier.stroke()
			
			image.unlockFocus()
			images.append( image )
		}
		
		return images
	}
	
	func colorizedImage(originalImage:NSImage, withTintColor tintColor:NSColor, alpha:CGFloat ) -> NSImage {
		
		let image = NSImage(size: originalImage.size)
		image.lockFocus()
		
		let rect = CGRectMake(0, 0, originalImage.size.width, originalImage.size.height);
		let ctx = NSGraphicsContext.currentContext()!.CGContext
		
		CGContextSetAlpha(ctx, alpha);
		CGContextSetFillColorWithColor(ctx, tintColor.CGColor);
		
		NSRectFill( rect );
		
		CGContextSetAlpha(ctx, 1.0);
		CGContextSetBlendMode(ctx, CGBlendMode.DestinationIn);
		
		var theRect = NSMakeRect(0,0, originalImage.size.width, originalImage.size.height);
		let cgimage:Unmanaged<CGImage>? = originalImage.CGImageForProposedRect(&theRect, context:nil ,hints:nil)
		
		CGContextDrawImage(ctx, rect, cgimage!.takeUnretainedValue());
		
		image.unlockFocus()
		
		return image;
		
	}
#endif


#if os(iOS)
	
	func ShowAlertMessage(title:String, _ message:String, _ inViewController:UIViewController!) {
		
		let alertController = UIAlertController(title: WLoc(title), message: WLoc(message), preferredStyle: .Alert)
		let cancel = UIAlertAction(title: WLoc("OK"), style: .Cancel) { (action) -> Void in
		}
		
		alertController.addAction(cancel)
		inViewController?.presentViewController(alertController, animated: true, completion: nil)
	}
	
	func loadingImages(tintColor:UIColor, size:CGFloat) -> [UIImage] {
		
		var images = [UIImage]()
		let size = CGSizeMake(size, size);
		
		let fraction:Double = 1.0/36.0 * 2.0 * M_PI
		
		for i in 0...36 {
			UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
			let ctx = UIGraphicsGetCurrentContext();
			
			let point = CGPointMake(15,15)
			let radius = CGFloat(12.0)
			let startAngle = CGFloat(Double(i) * fraction)
			let endAngle = CGFloat(fraction * 33) + startAngle
			let path = UIBezierPath(arcCenter: point, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
			
			path.lineWidth = 2.0
			CGContextSetStrokeColorWithColor(ctx, tintColor.CGColor)
			path.stroke()
			
			let image = UIGraphicsGetImageFromCurrentImageContext();
			UIGraphicsEndImageContext();
			
			images.append( image )
		}
		
		return images
	}
	
	func loadingImages(tintColor:UIColor) -> [UIImage] {
		return loadingImages(tintColor, size: 30.0)
	}
	
	extension UIImage {
		func colorizedImageWithTintColor(tintColor: UIColor, alpha: CGFloat, glow outerGlow: Bool) -> UIImage {
			UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.mainScreen().scale)
			let rect = CGRectMake(0, 0, self.size.width, self.size.height)
			let ctx = UIGraphicsGetCurrentContext()
			var t = CGAffineTransformIdentity
			t = CGAffineTransformScale(t, 1, -1)
			t = CGAffineTransformTranslate(t, 0, -rect.size.height)
			CGContextConcatCTM(ctx, t)
			CGContextSetAlpha(ctx, alpha)
			CGContextSetFillColorWithColor(ctx, tintColor.CGColor)
			UIRectFill(rect)
			
			CGContextSetAlpha(ctx, 1.0)
			CGContextSetBlendMode(ctx, .DestinationIn)
			CGContextDrawImage(ctx, rect, self.CGImage)
			var theImage = UIGraphicsGetImageFromCurrentImageContext()
			
			if outerGlow {
				CGContextSetShadowWithColor(ctx, CGSizeMake(0, 0), 3.0, tintColor.CGColor)
				CGContextSetBlendMode(ctx, .Normal)
				CGContextSetAlpha(ctx, 0.5)
				CGContextDrawImage(ctx, rect, theImage.CGImage)
				theImage = UIGraphicsGetImageFromCurrentImageContext()
			}
			UIGraphicsEndImageContext()
			return theImage
		}
	}
	
#endif

#if os(iOS)

class ObjCClass : NSObject {

	
	class func showAlertMessage(title:String, message:String, inViewController controller:UIViewController!) {
		
		ShowAlertMessage(title, message, controller)
	}
}

#endif
